---
- name: Containerd | Ensure containerd required directory exist
  file:
    path: "{{ item }}"
    state: directory
  with_items:
  - "{{ containerd_release_dir }}"
  - "{{ containerd_config_dir }}"

- name: Containerd | Download runc
  get_url:
    url: "{{ runc_release_url }}"
    mode: 0755
    dest: "{{ bin_dir }}/runc"
    force: yes
  notify: restart containerd

- name: Containerd | Download binaries
  unarchive:
    src: "{{ containerd_release_url }}"
    dest: "{{ containerd_release_dir }}"
    remote_src: yes
  notify: restart containerd

- name: Containerd | Drop systemd unit
  template:
    src: containerd.service.j2
    dest: /etc/systemd/system/containerd.service
  notify: restart containerd

- name: Containerd | Drop containerd config
  template:
    src: config.toml.j2
    dest: /etc/containerd/config.toml
  notify: restart containerd

- name: Containerd | Write crictl.yaml
  # no need for a template, as this is so small
  copy:
    dest: /etc/crictl.yaml
    content: |
      # debug: true
      # timeout: 30s
      runtime-endpoint: unix:///run/containerd/containerd.sock

- name: Containerd | Write ctr-namespace.sh
  copy:
    dest: /etc/profile.d/ctr-namespace.sh
    content: |
      CONTAINERD_NAMESPACE='k8s.io'
      export CONTAINERD_NAMESPACE

- name: Containerd | List containerd binaries
  find:
    paths: "{{ containerd_release_dir }}/bin"
  register: containerd_binaries
  notify: restart containerd

- name: Containerd | Symlink containerd binaries
  file:
    src: "{{ item.path }}"
    dest: "{{ bin_dir }}/{{ item.path | basename }}"
    state: link
  with_items: "{{ containerd_binaries.files }}"
  notify: restart containerd

- meta: flush_handlers

- name: Containerd | Enable Containerd unit
  systemd:
    name: containerd.service
    state: started
    enabled: yes
